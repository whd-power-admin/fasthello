from typing import List, Optional, Set
from datetime import date

from fastapi import FastAPI
from pydantic import BaseModel, HttpUrl

import logging

import pandas as pd

app = FastAPI()
logging.basicConfig(filename="example.log",
                    filemode='a',
                    format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                    datefmt='%H:%M:%S',
                    level=logging.DEBUG)


class Image(BaseModel):
    url: HttpUrl
    name: str


class Item(BaseModel):
    name: str
    description: Optional[str] = None
    price: float
    tax: Optional[float] = None
    tags: Set[str] = set()
    images: Optional[List[Image]] = None

class Predict_data(BaseModel):
    date: str
    block: int
    temp: float
    humidity: float
    weather: int
    wind: float
    contract_kw: float
    day_type: int

class Train_data(Predict_data):
    used_kw: float

class Train_body(BaseModel):
    area_id: str
    startDate: date
    startBloc: int
    endDate: date
    endBlock: int
    datas: List[Train_data]

class Prediction(BaseModel):
    block: int
    predict_kw: float

@app.get("/")
async def read_main():
    return {"msg": "Hello World"}
    
@app.put("/items/{item_id}")
async def update_item(item_id: int, item: Item):
    results = {"item_id": item_id, "item": item}
    return results

@app.post("/train")
async def get_train_datas(train_datas: Train_body):
    results = train_datas.datas
    logging.debug(train_datas.dict())
    logging.debug(train_datas.json())
    df1 = pd.read_json(train_datas.json())
    logging.debug(df1)
    return results